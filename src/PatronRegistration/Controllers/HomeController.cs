﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clc.Polaris.Api;
using Newtonsoft.Json;
using PatronRegistration.Models;
using PatronRegistration.Configuration;
using Clc.Polaris.Api.Models;
using System.Configuration;

namespace PatronRegistration.Controllers
{
    public class HomeController : Controller
    {
		public ActionResult RegisterSpl()
		{
			return Content("blank");
		}

        public string ViewIp()
        {
            return Request.UserHostAddress + "";
        }

        [HttpPost]
		public ActionResult RegisterSpl(PatronRegistrationParams p, int? day, int? month, int? year)
		{
			if (day != null)
			{
				if (month != null && year != null)
				{
					p.Birthdate = DateTime.Parse(string.Format("{0}/{1}/{2}", day, month, year)).Date;
				}
				else
				{
					return Json(new { Error = true, Message = "If specifying the date parts separately, all three parts must be present" });
				}
			}

			var polaris = Methods.CreatePapiClient();
			var result = polaris.PatronRegistrationCreate(p);

			if (result.Data?.Barcode != null)
			{
				return Content(JsonConvert.SerializeObject(new { PatronBarcode = result.Data?.Barcode, PatronID = result.Data?.PatronID }));
			}
			
			p.NameLast = string.Format("{0}-spl", p.NameLast);
			result = polaris.PatronRegistrationCreate(p);


			if (result.Data?.Barcode != null)
			{
				return Content(JsonConvert.SerializeObject(new { PatronBarcode = result.Data?.Barcode, PatronID = result.Data?.PatronID }));
			}

			
			return Content(JsonConvert.SerializeObject(new { Error = true, ErrorCode = result.Data?.PAPIErrorCode, ErrorMessage = result.Data?.ErrorMessage }));
		}

		public string RefreshSettings(string key)
		{
			if (key == ConfigurationManager.AppSettings["auth_key"]) { DbSettingProvider.RebuildCache(); return "ok"; }
			return "";
		}
    }
}

﻿using Clc.Polaris.Api;
using Newtonsoft.Json;
using PatronRegistration.Data;
using PatronRegistration.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using PatronRegistration.Configuration;
using PatronRegistration.Helpers;
using System.IO;
using System.Web.Script.Serialization;
using Clc.Polaris.Api.Models;
using NLog;
using Clc.Postmark;
using Clc.Postmark.Models;

namespace PatronRegistration.Controllers
{
    public class RegistrationController : Controller
    {
        PolarisEntities db;
        PapiClient polaris = Methods.CreatePapiClient();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        bool IsKidsForm => Session["k"] != null;

        public RegistrationController()
        {
            db = PolarisEntities.Create();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int? id, bool? dl, bool v = false, string t = "", string ipoverride = "", bool? autoReset = null)
        {
            if (!string.IsNullOrWhiteSpace(ipoverride)) { Session["ip"] = ipoverride; }
            else { Session["ip"] = Request.UserHostAddress; }

            bool showdl;
            if (dl.HasValue)
            {
                Session["hide_dl_button"] = dl.Value;
                showdl = dl.Value;
            }

            if (id.HasValue)
            {
                //Session["orgid"] = id.Value;
                Methods.SetUserOrgId(id.Value);
                if (autoReset.GetValueOrDefault()) { Session["reset_form"] = true; }

                if (t == "k") { Session["k"] = true; }

                bool.TryParse(Session["retry"]?.ToString(), out bool retry);
                if (!retry && t != "k")
                {
                    Session.Remove("k");
                }

                if (!v)
                {
                    var showWarning = polaris.SA_GetValueByOrg("PACPROF_PA_SELFREG_WARNPMT", id.Value);
                    if (showWarning.Data?.Value.ToLower() == "yes")
                    {
                        return RedirectToAction("Agreement", new { id = id.Value });
                    }
                }

                var session_hide_dl = Session["hide_dl_button"];
                if (session_hide_dl != null)
                {
                    showdl = (bool)session_hide_dl;
                }
                else
                {
                    var settings = new DbSettingProvider(id.Value);//  (.GetSettings(id.Value);

                    showdl = settings.EnableDriversLicenseSwipe && CheckIp(settings.DriversLicenseButtonEnabledIpAddresses);//.Any(i => Request.UserHostAddress.StartsWith(i));
                }

                logger.Trace("IP Address: {0}", Request.UserHostAddress);

                var model = BuildBaseRegistration(id.Value, showdl);

                HandleKidsForm(model);

                logger.Trace($"Library: {id.Value} | IP: {Request.UserHostAddress} | ShowDl: {showdl}");

                return View(model);
            }

            return RedirectToAction("SelectLibrary");
        }

        public ActionResult ManageAutoReset(bool? enable = null)
        {
            if (enable.HasValue)
            {
                if (enable.Value)
                {
                    var cookie = new HttpCookie("enableReset");
                    cookie.Expires = DateTime.Now.AddYears(99);
                    cookie.Value = "true";
                    Response.SetCookie(cookie);
                }
                else
                {
                    if (Request.Cookies["enableReset"] != null)
                    {
                        Response.Cookies["enableReset"].Expires = DateTime.Now.AddDays(-1);
                    }
                }
            }

            var currentlyEnabled = enable ?? Request.Cookies.AllKeys.Contains("enableReset");

            return View(currentlyEnabled);
        }

        public string ViewIp()
        {
            return Request.UserHostAddress;
        }

        bool CheckIp(IEnumerable<string> whitelist)
        {
            var ipToCheck = string.IsNullOrWhiteSpace(Session["ip"]?.ToString()) ? Request.UserHostAddress : Session["ip"].ToString();
            return whitelist.Any(i => ipToCheck.StartsWith(i));
        }

        public ActionResult SelectLibrary()
        {
            //Session["orgid"] = null;
            Methods.SetUserOrgId(null);
            return View(db.GetSelfRegistrationLibraries().OrderBy(b => b.Name).ToList());
        }

        Registration HandleSettings(Registration r, int orgId)
        {
            var settings = new DbSettingProvider(orgId);// ClcCustomSection.GetSettings(orgid.Value);
            r.Settings = settings;

            r.Genders = db.GetGendersToOrganizations(orgId).Where(g => g.Display).OrderBy(g => g.DisplayOrder).Select(g => new SelectListItem { Value = g.GenderID.ToString(), Text = g.Description }).ToList();

            if (r.Genders.Any(g => string.Equals(g.Text, "n/a", StringComparison.InvariantCultureIgnoreCase)))
            {
                r.Genders.FirstOrDefault(g => string.Equals(g.Text, "n/a", StringComparison.InvariantCultureIgnoreCase)).Text = settings.NaGenderText;
            }

            var patronBranch = db.Organizations.Single(o => o.OrganizationID == orgId);
            var library = patronBranch;

            if (r.PatronBranchID == 0)
            {
                if (patronBranch.OrganizationCodeID == 3)
                {
                    library = patronBranch.ParentOrganization;
                    r.PatronBranchID = orgId;
                }

                if (patronBranch.OrganizationCodeID == 2)
                {
                    r.PatronBranchID = library.ChildOrganizations.Min(o => o.OrganizationID);
                }
            }

            r.RequestPickupBranchID = r.RequestPickupBranchID.GetValueOrDefault(r.PatronBranchID);
            r.LogonUserID = settings.RegistrationLogonUserId;

            r.LibraryId = library.OrganizationID;

            r.Branches = new SelectList(db.GetSelfRegistrationBranches(r.LibraryId), "OrganizationID", "Name");
            r.PickupBranches = new SelectList(db.GetPickupBranches(r.LibraryId), "OrganizationID", "Name");

            //if (Request.Cookies.AllKeys.Contains("enableReset")) { r.ResetForm = true; }
            //if (((bool?)Session["reset_form"]).GetValueOrDefault()) { r.ResetForm = true; }

            return r;
        }

        public string DuplicateMessage(int id)
        {
            var settings = new DbSettingProvider(id);
            var branch = db.Organizations.Single(o => o.OrganizationID == id);

            if (branch.OrganizationCodeID == 2)
            {
                branch = db.Organizations.First(o => o.ParentOrganizationID == id);
            }

            var foo = db.Organizations.Where(o => new[] { o.OrganizationID, o.ParentOrganizationID ?? 0 }.Contains(id)).ToList().Where(o => !string.IsNullOrWhiteSpace(o.Phone)).OrderBy(o => o.OrganizationID).FirstOrDefault();

            var message = IsKidsForm ? settings.KidsFormDuplicateRegistrationText : settings.DuplicatePatronMessageHtml;// string.Format(settings.DuplicatePatronMessageHtml, branch.Phone, id);
            message = message.Replace("[branch_phone]", branch.Phone).Replace("[branch_id]", id.ToString());

            return message;
        }

        Registration BuildBaseRegistration(int orgid, bool showdl)
        {
            var model = new Registration
            {
                ShowDlButton = showdl
            };


            HandleSettings(model, orgid);

            if (model.Settings.DisplayMailingListCheckbox) { model.AddToMailingList = true; }


            //model.Branches.ForEach(b => { if (BranchDisplayNameOverrides.Any(o => o.Key == b.OrganizationID)) { b.Name = BranchDisplayNameOverrides.Single(o => o.Key == b.OrganizationID).Value; } });

            return model;
        }

        public ActionResult ViewSuccessPage(int orgid)
        {
            var model = new RegistrationSuccessModel()
            {
                DisplayText = GetRegistrationSuccessText("PACREG1234567", orgid),
                ResetForm = ShouldAutoReset(new DbSettingProvider(orgid)),
                Branch = orgid
            };

            return View("Success", model);
        }

        bool ShouldAutoReset(ISettingProvider settings)
        {
            var ip = Session["ip"]?.ToString();
            if (string.IsNullOrWhiteSpace(ip)) { ip = Request.UserHostAddress; }
            logger.Trace($"{ip} | Reset form setting: {settings.ResetForm} | Reset cookie: {Request.Cookies.AllKeys.Contains("enableReset")} | Reset session: {((bool?)Session["reset_form"]).GetValueOrDefault()}");
            return (settings.ResetForm && CheckIp(settings.DriversLicenseButtonEnabledIpAddresses)) || Request.Cookies.AllKeys.Contains("enableReset") || ((bool?)Session["reset_form"]).GetValueOrDefault();
        }

        [HttpGet]
        public ActionResult Agreement(int? id)
        {
            if (!id.HasValue)
            {
                return Redirect("https://catalog.clcohio.org");
            }

            //Session["orgid"] = id.Value;
            Methods.SetUserOrgId(id.Value);

            var orgSettings = new DbSettingProvider(id.Value);   

            var model = new RegistrationAgreementModel
            {
                OrganizationID = id.Value,
                WarningText = !string.IsNullOrWhiteSpace(orgSettings.WarningText) ? orgSettings.WarningText : db.GetLanuageString("pacprof_pa_warntext", id.Value),
                ConfirmButtonText = orgSettings.AgreementConfirmButtonText,
                CancelButtonText = orgSettings.AgreementCancelButtonText
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Agreement(bool answer, int OrganizationID)
        {
            if (answer)
            {
                //int.TryParse(Session["orgid"]?.ToString(), out int sessionOrg);
                var queryString = new { id = Methods.GetUserOrgId().GetValueOrDefault() > 0 ? Methods.GetUserOrgId().Value : OrganizationID, v = true, t = IsKidsForm ? "k" : null };
                return RedirectToAction("Create", queryString);
            }
            return Redirect("https://catalog.clcohio.org");
        }

        DateTime BuildBirthdate(int? day, int? month, int? year)
        {
            if (!day.HasValue || !month.HasValue || !year.HasValue) return DateTime.MinValue;

            var birthDate = DateTime.MinValue;
            if (!DateTime.TryParse(string.Format("{0}/{1}/{2}", month.ToString(), day.ToString(), year.ToString()), out birthDate))
            {
                return DateTime.MinValue;
            }

            return birthDate;
        }

        public string GetPhoneNumber(int orgid)
        {
            var phone = polaris.SA_GetValueByOrg("orgphone1", orgid);
            return phone.Data?.Value;
        }

        public ActionResult TestDupePage(string pn)
        {
            return View("DuplicateRegistration", (object)pn);
        }

        [HttpPost]
        public ActionResult Create(Registration p, int? day, int? month, int? year)
        {
            var settings = new DbSettingProvider(p.PatronBranchID);
            p.Settings = settings;
            var birthDate = BuildBirthdate(day, month, year);

            if (birthDate != DateTime.MinValue)
            {
                p.Birthdate = birthDate;
            }
            else
            {
                ModelState.AddModelError("", "Please enter a valid birth date.");
            }

            if (IsDupe(p))
            {
                var libPhone = GetPhoneNumber(p.PatronBranchID);

                var contactMsg = "Your registration is a possible duplicate of one already in the system. Please contact the library for more information";

                if (!string.IsNullOrWhiteSpace(libPhone))
                {
                    contactMsg = $"Your registration is a possible duplicate of one already in the system. Please contact the library at {libPhone} for more information";
                }

                ModelState.AddModelError("", contactMsg);
            }

            if (string.Equals(p.State, "ohio", StringComparison.InvariantCultureIgnoreCase)) p.State = "OH";

            if (!string.Equals(p.State, "oh", StringComparison.InvariantCultureIgnoreCase) && settings.BlockOutOfStateRegistrations)
            {
                ModelState.AddModelError("", "This location does not currently allow out of state registrations. Please contact the library for more information");
            }

            if (string.IsNullOrWhiteSpace(p.EmailAddress)) { p.EmailAddress = null; ModelState.Remove("EmailAddress"); }
            if (string.IsNullOrWhiteSpace(p.AltEmailAddress)) { p.AltEmailAddress = null; }

            if (p.DeliveryOptionID == 2 && string.IsNullOrWhiteSpace(p.EmailAddress))
            {
                ModelState.AddModelError("", "Please enter an email address or choose a different notification type.");
            }

            if (p.DeliveryOptionID == 3 && string.IsNullOrWhiteSpace(p.PhoneVoice1))
            {
                ModelState.AddModelError("", "Please enter a phone number or choose a different notification type.");
            }

            if (p.DeliveryOptionID == 8 && !p.TxtPhoneNumber.HasValue)
            {
                ModelState.AddModelError("", "Please select a TXT phone number or choose a different notification type.");
            }

            if (p.DeliveryOptionID == 8 && ((p.TxtPhoneNumber.GetValueOrDefault() == 1 && string.IsNullOrWhiteSpace(p.PhoneVoice1)) || (p.TxtPhoneNumber.GetValueOrDefault() == 2 && string.IsNullOrWhiteSpace(p.PhoneVoice2))))
            {
                ModelState.AddModelError("", "Please enter a TXT phone number or choose a different notification type.");
            }

            if (p.ReceiveEreceipts.GetValueOrDefault() == true && string.IsNullOrEmpty(p.EmailAddress))
            {
                ModelState.AddModelError("", "If you'd like to receive eReceipts please enter a valid email address.");
            }

            if (IsKidsForm && string.IsNullOrWhiteSpace(p.StreetTwo))
            {
                //ModelState.AddModelError("", "Parent/Guardian name is required");
            }

            if (!ModelState.IsValid)
            {
                //p.Branches = BuildBranchDropDown(p);
                Session["retry"] = true;
                HandleSettings(p, p.LibraryId);
                HandleKidsForm(p);
                return View("Create", p);
            }

            if (p.TxtPhoneNumber.HasValue)
            {
                switch (p.TxtPhoneNumber.Value)
                {
                    case 1:
                        p.Phone1CarrierID = 23;
                        break;
                    case 2:
                        p.Phone2CarrierID = 23;
                        break;
                }

                p.EnableSMS = true;
            }

            var parentOrg = db.Organizations.Single(o => o.OrganizationID == p.PatronBranchID).ParentOrganization;

            if (parentOrg.OrganizationID == 6)
            {
                p.User4 = "GHPA";
            }

            if (IsKidsForm)
            {
                if (parentOrg.OrganizationID == 28)
                {
                    p.PatronCode = 21;
                    p.NameLast = $"C-{p.NameLast}";
                    if (p.Birthdate.HasValue)
                    {
                        p.ExpirationDate = p.Birthdate.Value.AddYears(18);
                    }
                }
            }

            if (p.AddToMailingList)
            {
                p.User2 = "Newsletter";
            }

            if (p.ReceiveEreceipts.HasValue && p.ReceiveEreceipts.Value == true && !string.IsNullOrWhiteSpace(p.EmailAddress)) p.EReceiptOptionID = 2;

            if (p.ReceiveEreceipts == true)
            {
                if (string.IsNullOrWhiteSpace(p.EmailAddress) && p.EnableSMS == true) { p.EReceiptOptionID = 100; }
            }

            if (settings.NormalizeToUppercase)
            {
                p = NormalizeToUppercase(p);
            }

            if (p.TxtPhoneNumber.HasValue)
            {
                if (p.TxtPhoneNumber.GetValueOrDefault() == 1 && string.IsNullOrWhiteSpace(p.PhoneVoice1)) { p.TxtPhoneNumber = null; }
                if (p.TxtPhoneNumber.GetValueOrDefault() == 2 && string.IsNullOrWhiteSpace(p.PhoneVoice2)) { p.TxtPhoneNumber = null; }
            }

            p.UseLegalNameOnNotices = settings.UseLegalNameOnNotices && !string.IsNullOrWhiteSpace(p.LegalNameFirst);

            if (settings.DisplayECardCheckbox && p.IsECard) { p.Barcode = $"E{DateTimeOffset.Now.ToUnixTimeSeconds()}"; p.PatronCode = settings.EcardPatronCodeId; }

            if (settings.SchoolInfoFormat == "uapl" && p.IsECard) { p.User1 = ""; }

            if (!string.IsNullOrWhiteSpace(settings.SchoolInfoFormat))
            {
                if (p.uateacher.GetValueOrDefault()) { p.PatronCode = settings.TeacherPatronCodeId; }
                if (p.uastudent.GetValueOrDefault()) { p.PatronCode = settings.StudentPatronCodeId; }
            }

            PatronRegistrationParams _tmp = JsonConvert.DeserializeObject<PatronRegistrationParams>(JsonConvert.SerializeObject(p));
            var registration = polaris.PatronRegistrationCreate(_tmp);

            if (registration.Data?.PAPIErrorCode == -3528)
            {
                if (p.Settings.PerformPapiDupeBypass)
                {
                    if (p.Settings.UseFirstNameForDuplicateWorkaround)
                    {
                        _tmp.NameFirst += "-" + parentOrg.Abbreviation;
                    }
                    else
                    {
                        _tmp.NameLast += "-" + parentOrg.Abbreviation;
                    }

                    registration = polaris.PatronRegistrationCreate(_tmp);
                }

                if (registration.Data?.PAPIErrorCode == -3528)
                {
                    return View("DuplicateRegistration", (object)DuplicateMessage(p.PatronBranchID));
                }
            }

            if (string.IsNullOrWhiteSpace(registration.Data?.Barcode))
            {
                switch (registration.Data?.PAPIErrorCode)
                {
                    case -3510:
                        if (p.ZipMismatchRetry)
                        {
                            SendZipMismatchEmail(p.PatronBranchID, p.PostalCode, p.City);
                            return View("ZipMismatchError");
                        }
                        p.ZipMismatchRetry = true;
                        ModelState.AddModelError("", "ZIP mismatch. Please double-check your address information and try again.");
                        break;
                    case -3511:
                        goto case -3510;
                    case -3512:
                        goto case -3510;
                    case -3513:
                        goto case -3510;
                    case -3514:
                        goto case -3510;
                    case -3515:
                        goto case -3510;
                    case -3516:
                        goto case -3510;
                    case -3517:
                        goto case -3510;
                    default:
                        logger.Error($"Error message: {registration?.Data.ErrorMessage}\r\nRegistration Data: {JsonConvert.SerializeObject(registration)}");
                        return View("Failure", registration.Data?.PAPIErrorCode);
                }

                if (!ModelState.IsValid)
                {
                    p = HandleSettings(p, p.PatronBranchID);
                    return View("Create", p);
                }
            }

            var registrationText = GetRegistrationSuccessText(registration.Data.Barcode, p.PatronBranchID, p.IsECard);
            logger.Trace($"PatronID: {registration.Data.PatronID} | IP Address: {Request.UserHostAddress}");

            var model = new RegistrationSuccessModel
            {
                DisplayText = registrationText,
                Branch = p.PatronBranchID,
                ResetForm = ShouldAutoReset(settings)
            };

            if (p.AddToMailingList)
            {
                if (settings.MailingListRecordSetId > 0)
                {
                    polaris.RecordSetContentAdd(settings.MailingListRecordSetId, registration.Data.PatronID);
                }
            }

            if (p.IsECard)
            {
                SendEcardWelcomeEmail(p);
            }

            if (!string.Equals(p.State, "oh", StringComparison.InvariantCultureIgnoreCase))
            {
                if (settings.ChargeForOutOfStateRegistrations)
                {
                    ModelState.AddModelError("", settings.RegistrationChargeText);
                    model.DisplayText = $"{settings.RegistrationChargeText}<br/><br/>{model.DisplayText}";
                }
            }

            if (IsKidsForm)
            {
                if (settings.KidsFormRecordSetId > 0)
                {
                    polaris.RecordSetContentAdd(settings.KidsFormRecordSetId, registration.Data.PatronID);
                }
            }

            Session.RemoveAll();
            Methods.SetUserOrgId(model.Branch);
            Session["reset_form"] = model.ResetForm;

            return View("Success", model);

        }

        void SendEcardWelcomeEmail(Registration patron)
        {
            if (string.IsNullOrWhiteSpace(patron.Settings.EcardEmailTemplate)) { return; }

            var postmark = new PostmarkClient2(patron.Settings.PostmarkApiKey);
            var textBody = patron.Settings.EcardEmailTemplate.Replace("[barcode]", patron.Barcode);
            var result = postmark.Send(patron.EmailAddress, $"{patron.Settings.EcardEmailFromName} <donotreply@clcohio.org>", patron.Settings.EcardEmailSubject, textBody: textBody);
            var foo = result;
        }

        string GetRegistrationSuccessText(string barcode, int patronBranch, bool isEcard = false)
        {
            var settings = new DbSettingProvider(patronBranch);
            //var registrationText = IsKidsForm ? settings.KidsFormRegistrationText : settings.RegistrationText;
            var registrationText = settings.RegistrationText;

            if (ShouldAutoReset(settings) && !string.IsNullOrWhiteSpace(settings.KioskRegistrationText))
            {
                registrationText = settings.KioskRegistrationText;
            }

            if (IsKidsForm && !string.IsNullOrWhiteSpace(settings.KidsFormRegistrationText))
            {
                registrationText = settings.KidsFormRegistrationText;
            }

            if (isEcard) { registrationText = settings.EcardRegistrationText; }

            if (string.IsNullOrWhiteSpace(registrationText))
            {
                var text = db.GetLanuageString("pacml_submitselfreg_1781", patronBranch);
                registrationText = $"{text} <span id=\"patron-barcode\">{barcode}</span>";
            }
            else
            {
                registrationText = registrationText.Replace("[barcode]", barcode);
            }

            return registrationText;
        }

        Registration HandleKidsForm(Registration r)
        {
            if (IsKidsForm)
            {
                var settings = new DbSettingProvider(r.PatronBranchID);
                r.Address2LabelOverride = settings.KidsFormStreetTwoLabel;
            }

            return r;
        }

        Registration NormalizeToUppercase(Registration r)
        {
            r.NameFirst = r.NameFirst?.ToUpper();
            r.NameLast = r.NameLast?.ToUpper();
            r.NameMiddle = r.NameMiddle?.ToUpper();
            r.LegalNameFirst = r.LegalNameFirst?.ToUpper();
            r.LegalNameMiddle = r.LegalNameMiddle?.ToUpper();
            r.LegalNameLast = r.LegalNameLast?.ToUpper();
            r.EmailAddress = r.EmailAddress?.ToUpper();
            r.AltEmailAddress = r.AltEmailAddress?.ToUpper();
            r.StreetOne = r.StreetOne?.ToUpper();
            r.StreetTwo = r.StreetTwo?.ToUpper();
            r.City = r.City?.ToUpper();
            r.State = r.State?.ToUpper();

            return r;
        }

        void SendZipMismatchEmail(int orgId, string zip, string city)
        {
            var library = db.Organizations.Single(o => o.OrganizationID == orgId).ParentOrganizationID;
            var userId = db.PolarisUserLists.Single(u => u.OrganizationID == library && u.KindOfList == 0).PolarisUserID;
            var email = db.UsersPPPPs.Single(u => u.PolarisUserID == userId && u.AttrID == 658).Value;

            var body = string.Format("Invalid ZIP/City combination entered: {0} {1}", city, zip);
            var postmark = new PostmarkClient(AppSettings.PostmarkApiKey);
            var result = postmark.Send(email, AppSettings.EmailFromAddress, "Invalid ZIP/City Combination", body);
        }

        public bool DupeCheck(Registration reg)
        {
            if (AppSettings.AlwaysDupe) { return true; }
            if (reg.PatronBranchID == 0) { return false; }

            var settings = new DbSettingProvider(reg.PatronBranchID);
            if (IsKidsForm)
            {
                if (settings.KidsFormBypassDupeCheck) return false;
            }
            else
            {
                if (settings.BypassDupeCheck) return false;
            }
            if (reg.PatronBranchID == 0 || !reg.Day.HasValue || !reg.Month.HasValue || !reg.Year.HasValue || string.IsNullOrWhiteSpace(reg.NameLast)) return false;
            return IsDupe(reg);
        }

        public string AgeCheck(Registration reg)
        {
            var patronBranch = reg?.PatronBranchID ?? 0;
            var settings = new DbSettingProvider(patronBranch);

            if (patronBranch == 0 || !settings.EnableAgeWarning || !reg.Day.HasValue || !reg.Month.HasValue || !reg.Year.HasValue) { return ""; }

            DateTime.TryParse($"{reg.Month}/{reg.Day}/{reg.Year}", out DateTime birthDate);

            if ((DateTime.Now.Subtract(birthDate).Days / 365) < 18)
            {
                return settings.AgeWarningText;
            }

            return "";
        }

        public JsonResult dl(string dlinfo, string format)
        {
            if (string.IsNullOrWhiteSpace(dlinfo) || dlinfo == "null")
            {
                return Json("");
            }

            if (format == "barcode")
            {
                return Json(ProcessDLBarcode(dlinfo));
            }

            return Json(ProcessDL(dlinfo));
        }

        bool IsDupe(Registration reg)
        {
            var library = db.Organizations.Single(o => o.OrganizationID == reg.PatronBranchID).ParentOrganizationID;

            var birthDate = BuildBirthdate(reg.Day, reg.Month, reg.Year);

            if (birthDate != DateTime.MinValue)
            {
                reg.Birthdate = birthDate;
            }

            var lastname = reg.NameLast;

            if (IsKidsForm && library == 28)
            {
                lastname = "C-" + lastname;
            }

            var patrons = (db.Patrons.Where(p =>
                p.PatronRegistration.NameFirst.Substring(0, 3) == reg.NameFirst.Substring(0, 3) &&
                p.PatronRegistration.NameLast == lastname &&
                library == p.Organization.ParentOrganizationID &&
                (p.PatronRegistration.Birthdate.HasValue ? DbFunctions.TruncateTime(p.PatronRegistration.Birthdate.Value) : DateTime.MinValue.Date) == DbFunctions.TruncateTime(reg.Birthdate.Value)
                )).ToList();

            return patrons.Any();
        }

        public ContentResult DLJSONP(string data, string format)
        {
            var json = "";
            if (format == "barcode")
            {
                json = new JavaScriptSerializer().Serialize(ProcessDLBarcode(data));
            }
            else
            {
                json = new JavaScriptSerializer().Serialize(ProcessDL(data));
            }
            return Content($"callback({json});", "application/javascript");
        }

        DriversLicenseInfo ProcessDL(string data)
        {
            var output = new DriversLicenseInfo();
            data = data.Remove(0, 1);

            output.State = new string(data.Take(2).ToArray());

            data = data.Remove(0, 2);
            if (data.Take(13).Contains('^'))
            {
                output.City = data.Split('^')[0];
                data = data.Remove(0, output.City.Length + 1);
            }
            else
            {
                output.City = new string(data.Take(13).ToArray());
                data = data.Remove(0, 13);
            }

            output.LastName = data.Split('$')[0];
            output.FirstName = data.Split('$')[1];
            //output.MiddleName = data.Contains('+') ? "" : data.Split('$')[2];

            var bdParse = new DateTime();

            var birthdateData = Regex.Match(data, @"(?<=[=])(.*)(?=\?)").Value.Substring(4, 8);
            if (DateTime.TryParseExact(birthdateData, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdParse))
            {
                output.BirthMonth = bdParse.Month;
                output.BirthDay = bdParse.Day;
                output.BirthYear = bdParse.Year;
            }

            output.Address = data.Split('^')[1];
            if (data.Contains('#'))
            {
                output.ZIP = new string(data.Split('#')[1].Skip(2).Take(5).ToArray());
                var gender = new string(data.Split('#')[1].Skip(29).Take(1).ToArray());

                output.Gender = gender == "1" ? "M" : gender == "2" ? "F" : "";

            }
            else if (data.Contains("+"))
            {
                output.ZIP = new string(data.Split('+')[1].Skip(2).Take(5).ToArray());
            }
            return output;
        }

        DriversLicenseInfo ProcessDLBarcode(string data)
        {
            var output = new DriversLicenseInfo();

            output.LastName = Regex.Match(data, "DCS(.*?)DAC").Groups[1].Value;
            output.FirstName = Regex.Match(data, "DAC(.*?)DAD").Groups[1].Value;
            //output.MiddleName = Regex.Match(data, "DAD(.*?)DBD").Groups[1].Value.Substring(0, 1);
            output.Address = Regex.Match(data, "DAG(.*?)DAI").Groups[1].Value;
            output.City = Regex.Match(data, "DAI(.*?)DAJ").Groups[1].Value;
            output.State = Regex.Match(data, "DAJ(.*?)DAK").Groups[1].Value;
            output.ZIP = Regex.Match(data, @"DAK(.*?)\s").Groups[1].Value.Substring(0, 5);

            var bdParse = new DateTime();
            var birthdateData = Regex.Match(data, "DBB(.*?)DBC").Groups[1].Value;
            if (DateTime.TryParseExact(birthdateData, "MMddyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bdParse))
            {
                output.BirthMonth = bdParse.Month;
                output.BirthDay = bdParse.Day;
                output.BirthYear = bdParse.Year;
            }

            var gender = Regex.Match(data, @"DBC(.*?)DAY").Groups[1].Value;
            output.Gender = gender == "1" ? "M" : gender == "2" ? "F" : "";

            return output;
        }
    }

    public class DriversLicenseInfo
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public int BirthMonth { get; set; }
        public int BirthDay { get; set; }
        public int BirthYear { get; set; }
    }
}

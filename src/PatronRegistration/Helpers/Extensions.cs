﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PatronRegistration
{
    public static class Extensions
    {
        public static T GetPropertyValue<T>(this object obj, string property)
        {
            return (T)obj.GetType().GetProperty(property)?.GetValue(obj, null);
        }

        public static string FormatTemplate(this object obj, string template)
        {
            var output = template;
            var matches = Regex.Matches(template, "{{(.*?)}}");
            foreach (Match match in matches)
            {
                var value = obj.GetPropertyValue<string>(match.Groups[1].Value);
                output = output.Replace(match.Groups[0].Value, value);
            }

            return output;
        }
    }
}
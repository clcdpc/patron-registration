﻿using PatronRegistration.Configuration;
using PatronRegistration.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatronRegistration.Helpers
{
    public class CustomCssHelper
    {
        public static MvcHtmlString CustomCss(int orgId)
        {
            if (orgId < 2) return null;

            var settings = new DbSettingProvider(orgId);
            string formatString = "<link rel=\"stylesheet\" type=\"text/css\" href=\"/Content/Custom/{0}\" />";

            if (!string.IsNullOrWhiteSpace(settings.CssFile))
            {
                return MvcHtmlString.Create(string.Format(formatString, settings.CssFile));
            }

            return null;
        }
    }
}
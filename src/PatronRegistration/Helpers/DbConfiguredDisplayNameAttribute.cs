﻿using PatronRegistration.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace PatronRegistration.Helpers
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class DbConfiguredDisplayNameAttribute : DisplayNameAttribute
    {
        public override string DisplayName
        {
            get
            {

                //var settings = new DbSettingProvider(((int?)HttpContext.Current.Session["orgid"]).GetValueOrDefault());
                var settings = new DbSettingProvider(Methods.GetUserOrgId().GetValueOrDefault());

                var key = $"displayname_{TheType.FullName}.{PropertyName}";
                var value = settings.GetSetting<string>(key);
                return string.IsNullOrWhiteSpace(value) ? PropertyName : value;
            }
        }

        string PropertyName = "";
        Type TheType;

        public DbConfiguredDisplayNameAttribute(Type type, [CallerMemberName] string propertyName = null)
        {
            PropertyName = propertyName;
            TheType = type;
        }
    }
}
﻿using Clc.Polaris.Api;
using Clc.Polaris.Api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PatronRegistration
{
    public static class Methods
    {
        static string orgidCookie = "orgid";
        public static PapiClient CreatePapiClient()
        {
            return new PapiClient
            {
                AccessID = ConfigurationManager.AppSettings["papi_user"],
                AccessKey = ConfigurationManager.AppSettings["papi_key"],
                Hostname = ConfigurationManager.AppSettings["papi_request_hostname"],
                StaffOverrideAccount = new PolarisUser
                {
                    Domain = ConfigurationManager.AppSettings["staff_domain"],
                    Username = ConfigurationManager.AppSettings["staff_username"],
                    Password = ConfigurationManager.AppSettings["staff_password"],
                }
            };
        }

        public static void SetUserOrgId(int? orgId)
        {
            HttpContext.Current.Request.Cookies.Remove(orgidCookie);
            HttpContext.Current.Session[orgidCookie] = orgId;
            if (orgId.HasValue)
            {
                HttpContext.Current.Request.Cookies.Add(new HttpCookie(orgidCookie, orgId.ToString()) { Expires = DateTime.Now.AddYears(1) });
            }
        }

        public static int? GetUserOrgId()
        {
            var cookieVal = HttpContext.Current.Request.Cookies.Get(orgidCookie)?.Value;
            
            if (int.TryParse(cookieVal, out int orgId)) { return orgId; }

            var sessionVal = (int?)HttpContext.Current.Session[orgidCookie];
            return sessionVal;
        }
    }
}
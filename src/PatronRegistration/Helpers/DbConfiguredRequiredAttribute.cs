﻿using PatronRegistration.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace PatronRegistration.Helpers
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class DbConfiguredRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        public string BooleanSwitch { get; private set; }
        public bool AllowEmptyStrings { get; private set; }
        public string Mnemonic { get; set; }

        public DbConfiguredRequiredAttribute(string mnemonic) : base("The {0} field is required.")
        {
            Mnemonic = mnemonic;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // switch this to PatronBranchId at some point, requires fixing "pick branch" option
            PropertyInfo property = validationContext.ObjectType.GetProperty("LibraryId");
            var patronBranch = (int)property.GetValue(validationContext.ObjectInstance);
            var settings = new DbSettingProvider(patronBranch);

            if (settings.GetSetting(Mnemonic, false) && string.IsNullOrWhiteSpace(value?.ToString()))
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            object model = context.Controller.ViewData.Model;
            var patronBranch = (int)model.GetType().GetProperty("LibraryId").GetValue(model, null);
            
            var settings = new DbSettingProvider(patronBranch);

            if (settings.GetSetting(Mnemonic, false))
            {
                yield return new ModelClientValidationRequiredRule(FormatErrorMessage(metadata.DisplayName ?? metadata.PropertyName));
            }
            else
            //we have to return a ModelCLientValidationRule where
            //ValidationType is not empty or else we get an exception
            //since we don't add validation rules clientside for 'notrequired'
            //no validation occurs and this works, though it's a bit of a hack
            {
                yield return new ModelClientValidationRule { ValidationType = "notrequired", ErrorMessage = "" };
            }
        }
    }
}
﻿using PatronRegistration.Configuration;
using PatronRegistration.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatronRegistration.Helpers
{
    public class HeaderImageHelper
    {
        public static MvcHtmlString HeaderImage(int orgId)
        {
            var settings = new DbSettingProvider(orgId);
            string formatString = "<img src=\"{0}\" alt=\"header-image\" />";

            if (!string.IsNullOrWhiteSpace(settings.HeaderImageUrl))
            {
                return MvcHtmlString.Create(string.Format(formatString, settings.HeaderImageUrl));
            }

            return null;
        }
    }
}
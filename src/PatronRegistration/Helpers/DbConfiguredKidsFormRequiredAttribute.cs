﻿using PatronRegistration.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace PatronRegistration.Helpers
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class DbConfiguredKidsFormRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        public string BooleanSwitch { get; private set; }
        public bool AllowEmptyStrings { get; private set; }
        public string Mnemonic { get; set; }

        public DbConfiguredKidsFormRequiredAttribute() : base("The {0} field is required.")
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (HttpContext.Current.Session["k"] != null)
            {
                PropertyInfo property = validationContext.ObjectType.GetProperty("LibraryId");
                var patronBranch = (int)property.GetValue(validationContext.ObjectInstance);
                if (patronBranch == 0) { return new ValidationResult(""); }

                var settings = new DbSettingProvider(patronBranch);

                if (settings.KidsFormRequireStreetTwo && string.IsNullOrWhiteSpace(value as string))
                {
                    return new ValidationResult(FormatErrorMessage(string.IsNullOrWhiteSpace(settings.KidsFormStreetTwoLabel) ? validationContext.DisplayName : settings.KidsFormStreetTwoLabel));
                }
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            //new[] { new ModelClientValidationRule { ErrorMessage = "<Your error message>", ValidationType = "required" } };
            object model = context.Controller.ViewData.Model;
            var patronBranch = (int)model.GetType().GetProperty("LibraryId").GetValue(model, null);

            var settings = new DbSettingProvider(patronBranch);

            if (HttpContext.Current.Session["k"] != null && settings.KidsFormRequireStreetTwo)
            {
                yield return new ModelClientValidationRequiredRule(FormatErrorMessage(settings.KidsFormStreetTwoLabel));
            }
            else
            //we have to return a ModelCLientValidationRule where
            //ValidationType is not empty or else we get an exception
            //since we don't add validation rules clientside for 'notrequired'
            //no validation occurs and this works, though it's a bit of a hack
            {
                yield return new ModelClientValidationRule { ValidationType = "notrequired", ErrorMessage = "" };
            }
        }
    }
}
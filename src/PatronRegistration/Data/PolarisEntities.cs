﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PatronRegistration.Data
{
    public partial class PolarisEntities : DbContext
    {
        public PolarisEntities(string server, string database)
            : base(GetConnectionString(server, database, "Data.PolarisModel"))
        {
        }

        static PolarisEntities FromConfig()
        {
            var server = ConfigurationManager.AppSettings["db_server"];
            return new PolarisEntities(server, "Polaris");
        }

        public static PolarisEntities Create()
        {
            return FromConfig();
        }

        static string GetConnectionString(string server, string database, string metadata)
        {
            // Specify the provider name, server and database.
            string providerName = "System.Data.SqlClient";

            // Initialize the connection string builder for the
            // underlying provider.
            SqlConnectionStringBuilder sqlBuilder =
                new SqlConnectionStringBuilder();

            // Set the properties for the data source.
            sqlBuilder.DataSource = server;
            sqlBuilder.InitialCatalog = database;
            sqlBuilder.IntegratedSecurity = true;

            // Build the SqlConnection connection string.
            string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            EntityConnectionStringBuilder entityBuilder =
                new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = providerString;

            // Set the Metadata location.
            entityBuilder.Metadata = string.Format("res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", metadata);

            return entityBuilder.ToString();
        }

        public IEnumerable<Organization> GetSelfRegistrationBranches(int? libraryId = null)
        {
            var branches = Organizations.Where(o => o.OrganizationCodeID == 3 && (o.OrganizationsPPPPs.FirstOrDefault(p => p.AttrID == 1376) ?? o.ParentOrganization.OrganizationsPPPPs.FirstOrDefault(p => p.AttrID == 1376)).Value == "yes");
            if (libraryId.HasValue)
            {
                branches = branches.Where(b => b.ParentOrganizationID == libraryId);
            }

            var disabledBranches = CLC_Custom_Registration_Form_Settings.Where(s => s.Setting == "disable_branch").Select(s => s.OrganizationID);

            branches = branches.Where(b => !disabledBranches.Contains(b.OrganizationID));

            var branches2 = branches.ToList().GroupJoin(
                    GetBranchDisplayNames(),
                    b => b.OrganizationID,
                    n => n.Key,
                    (b, n) => new Organization { OrganizationID = b.OrganizationID, Name = string.IsNullOrWhiteSpace(n.FirstOrDefault().Value) ? b.Name : n.FirstOrDefault().Value });

            return branches2;
        }

        public IEnumerable<Organization> GetPickupBranches(int? libraryId = null)
        {
            var selfRegBranches = GetSelfRegistrationBranches(libraryId).Select(o => o.OrganizationID);

            var pickupBranches = CLC_Custom_Registration_Form_Settings.Where(s => s.Setting == "enable_as_pickup_branch").ToList();
            if (libraryId.HasValue)
            {
                var branches = Organizations.Where(o => o.ParentOrganizationID == libraryId).Select(b => b.OrganizationID).ToList();
                pickupBranches = pickupBranches.Where(b => branches.Contains(b.OrganizationID)).ToList();
            }

            return Organizations.Where(o => o.OrganizationCodeID == 3).ToList().Where(o => pickupBranches.Select(pb => pb.OrganizationID).Contains(o.OrganizationID) || selfRegBranches.Contains(o.OrganizationID));
        }

        public Dictionary<int, string> GetBranchDisplayNames()
        {
            return CLC_Custom_Registration_Form_Settings.Where(s => s.Setting == "branch_display_name").ToDictionary(s => s.OrganizationID, s => s.Value);
        }

        public IQueryable<Organization> GetSelfRegistrationLibraries()
        {
            var branches = Organizations.Where(o => o.OrganizationCodeID == 2 && ((o.OrganizationsPPPPs.FirstOrDefault(p => p.AttrID == 1376) ?? o.ParentOrganization.OrganizationsPPPPs.FirstOrDefault(p => p.AttrID == 1376)).Value == "yes" || o.ChildOrganizations.Any(o2 => o2.OrganizationsPPPPs.Any(p => p.AttrID == 1376 && p.Value == "yes"))));
            return branches;
        }

        public string GetLanuageString(string mnemonic, int orgId)
        {
            if (AdminAttributes.Any(a => a.Mnemonic == mnemonic))
            {
                return LoadAdminLanguageString(mnemonic, orgId);
            }
            else
            {
                return LoadMultiLanguageString(mnemonic, orgId);
            }
        }

        private string LoadMultiLanguageString(string mnemonic, int orgId)
        {
            var value = SA_CustomMultiLingualStrings.SingleOrDefault(s => s.Mnemonic == mnemonic && s.OrganizationID == orgId && s.LanguageID == 1033)?.Value;

            if (value == null)
            {
                if (orgId == 1) return AdminAttributes.Single(a => a.Mnemonic == mnemonic).DefaultValue;
                var org = Organizations.Single(o => o.OrganizationID == orgId);
                value = LoadMultiLanguageString(mnemonic, org.ParentOrganizationID ?? 1);
            }

            return value;
        }

        private string LoadAdminLanguageString(string mnemonic, int orgId)
        {
            var attr = AdminAttributes.Single(a => a.Mnemonic == mnemonic);
            var value = AdminLanguageStrings.SingleOrDefault(s => s.AdminAttrID == attr.AttrID && s.OrganizationID == orgId && s.AdminLanguageID == 1033)?.Value;

            if (value == null)
            {
                if (orgId == 1) return AdminAttributes.Single(a => a.Mnemonic == mnemonic).DefaultValue;
                var org = Organizations.Single(o => o.OrganizationID == orgId);
                value = LoadAdminLanguageString(mnemonic, org.ParentOrganizationID ?? 1);
            }

            return value;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatronRegistration.Data
{
    public partial class Organization
    {
        public string Phone => OrganizationsPPPPs.SingleOrDefault(op => op.Mnemonic.ToLower() == "orgphone1")?.Value ?? "";
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PatronRegistration.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CLC_Custom_OrganizationsPPPP
    {
        public int OrganizationID { get; set; }
        public Nullable<int> ParentOrganizationID { get; set; }
        public int OrganizationCodeID { get; set; }
        public string Name { get; set; }
        public int AttrID { get; set; }
        public string Mnemonic { get; set; }
        public string AttrDesc { get; set; }
        public string Value { get; set; }
    
        public virtual Organization Organization { get; set; }
    }
}

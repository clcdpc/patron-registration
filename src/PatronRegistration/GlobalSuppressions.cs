﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:PatronRegistration.Controllers.RegistrationController.DuplicateMessage(System.Int32)~System.String")]
[assembly: SuppressMessage("Performance", "CA1806:Do not ignore method results", Justification = "<Pending>", Scope = "member", Target = "~M:PatronRegistration.Controllers.RegistrationController.Create(System.Nullable{System.Int32},System.Nullable{System.Boolean},System.Boolean,System.String,System.String,System.Nullable{System.Boolean})~System.Web.Mvc.ActionResult")]
[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "<Pending>", Scope = "member", Target = "~M:PatronRegistration.Controllers.RegistrationController.Create(System.Nullable{System.Int32},System.Nullable{System.Boolean},System.Boolean,System.String,System.String,System.Nullable{System.Boolean})~System.Web.Mvc.ActionResult")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>", Scope = "member", Target = "~M:PatronRegistration.Controllers.RegistrationController.CheckIp(System.Collections.Generic.IEnumerable{System.String})~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:PatronRegistration.Controllers.HomeController.RegisterSpl(Clc.Polaris.Api.Models.PatronRegistrationParams,System.Nullable{System.Int32},System.Nullable{System.Int32},System.Nullable{System.Int32})~System.Web.Mvc.ActionResult")]

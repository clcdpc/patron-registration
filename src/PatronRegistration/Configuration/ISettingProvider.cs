﻿using PatronRegistration.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace PatronRegistration.Configuration
{
    public interface ISettingProvider
    {
        int OrganizationId { get; set; }

        string HeaderImageUrl { get; }
        string CssFile { get; }
        string WarningText { get; }
        string CustomFormFooterHtml { get; }
        IEnumerable<string> DriversLicenseButtonEnabledIpAddresses { get; }
        bool ResetForm { get; }
        bool EnableDriversLicenseSwipe { get; }
        bool HideGender { get; }
        bool HideEreceipt { get; }
        bool RequireEreceipt { get; }
        string NaGenderText { get; }
        bool NormalizeToUppercase { get; }
        string DriversLicenseFormat { get; }
        bool BypassDupeCheck { get; }
        bool PerformPapiDupeBypass { get; }
        bool EnablePatronBranchSelectOption { get; }
        string RegistrationText { get; }
        bool BlockOutOfStateRegistrations { get; }
        bool ChargeForOutOfStateRegistrations { get; }
        string RegistrationChargeText { get; }
        string RegistrationHeader { get; }
        bool RequireEmail { get; }
        bool RequirePhone1 { get; }
        string KidsFormStreetTwoLabel { get; }
        bool KidsFormRequireStreetTwo { get; }
        int KidsFormRecordSetId { get; }
        bool KidsFormBypassDupeCheck { get; }
        string KidsFormRegistrationText { get; }
        string KidsFormDuplicateRegistrationText { get; }
        bool EnableLegalNameCheckbox { get; }
        string LegalNameCheckboxLabel { get; }
        bool UseLegalNameOnNotices { get; }
        bool EnableAgeWarning { get; }
        string AgeWarningText { get; }
        string DriversLicenseButtonText { get; }
        string DriversLicensePromptText { get; }
        string AgreementConfirmButtonText { get; }
        string AgreementCancelButtonText { get; }
        string KioskRegistrationText { get; }
        string KioskRegistrationHeader { get; }
        string SchoolInfoFieldLegend { get; }
        bool DisplayECardCheckbox { get; }
        string ECardCheckboxLabel { get; }
        bool DisplayMailingListCheckbox { get; }
        bool DisplayPreferredPickupLocation { get; }
        bool RequirePreferredPickupLocation { get; }
        bool DisplayResponsiblePersonField { get; }
        bool UseFirstNameForDuplicateWorkaround { get; }
        string MailingListCheckboxLabel { get; }
        string MailingListDescriptionHtml { get; }
        int MailingListRecordSetId { get; }
        int RegistrationLogonUserId { get; }
        int EcardPatronCodeId { get; }
        int TeacherPatronCodeId { get; }
        int StudentPatronCodeId { get; }
        string SchoolInfoFormat { get; }
        string ResponsiblePersonDisclaimer { get; }
        string EcardRegistrationText { get; }
        string EcardEmailTemplate { get; }
        string EcardEmailFromName { get; }
        string EcardEmailSubject { get; }
        string PostmarkApiKey { get; }
    }

    public class DbSettingProvider : ISettingProvider
    {
        public int OrganizationId { get; set; }
        public int LibraryId { get; set; }
        static List<CLC_Custom_Registration_Form_Settings> SettingsCache = null;
        static List<Organization> OrganizationCache = null;

        public DbSettingProvider(int orgId)
        {
            OrganizationId = orgId;
            if (SettingsCache == null)
            {
                Debug.WriteLine("Hitting settings table.");
                using (var db = PolarisEntities.Create()) { SettingsCache = db.CLC_Custom_Registration_Form_Settings.ToList(); }
            }
            if (OrganizationCache == null)
            {
                Debug.WriteLine("Hitting organizations table.");
                using (var db = PolarisEntities.Create()) { OrganizationCache = db.Organizations.ToList(); }
            }
            LibraryId = OrganizationCache.Single(o => o.OrganizationID == OrganizationId).ParentOrganizationID.Value;
        }

        public T GetSetting<T>(string name, T defaultValue = default(T))
        {
            var test = SettingsCache.Where(s => new[] { OrganizationId, LibraryId, 1 }.Contains(s.OrganizationID) && s.Setting == name).OrderByDescending(s => s.OrganizationID).FirstOrDefault();
            if (test?.Value == null) { return defaultValue; }
            return (T)Convert.ChangeType(test.Value, typeof(T));
        }

        public static void RebuildCache()
        {
            using (var db = PolarisEntities.Create())
            {
                SettingsCache = db.CLC_Custom_Registration_Form_Settings.ToList();
            }
        }

        public bool ChargeForOutOfStateRegistrations => GetSetting<bool>("charge_for_out_of_state_registrations");

        public string HeaderImageUrl => GetSetting<string>("header_image_url");

        public string CssFile => GetSetting<string>("css_file");

        public string WarningText => GetSetting<string>("warning_text");

        public string CustomFormFooterHtml => GetSetting<string>("custom_form_footer_html");

        public IEnumerable<string> DriversLicenseButtonEnabledIpAddresses
        {
            get
            {
                var value = GetSetting<string>("show_dl_ips");
                return string.IsNullOrWhiteSpace(value) ? new List<string>() : value.Split(';').ToList();
            }
        }

        public bool ResetForm => GetSetting<bool>("reset_form");

        public bool EnableDriversLicenseSwipe => GetSetting<bool>("show_dl");

        public bool HideGender => GetSetting<bool>("hide_gender");

        public bool EnableAgeWarning => GetSetting<bool>("enable_age_warning");

        public string AgeWarningText => GetSetting<string>("age_warning_text");

        public bool HideEreceipt => GetSetting<bool>("hide_ereceipt");

        public bool RequireEreceipt => GetSetting<bool>("require_ereceipt");

        public string NaGenderText => GetSetting<string>("na_gender_text");

        public bool NormalizeToUppercase => GetSetting<bool>("normalize_to_uppercase");

        public string DriversLicenseFormat => GetSetting<string>("dl_format");

        public bool BypassDupeCheck => GetSetting<bool>("bypass_dupe_check");

        public bool RequireEmail => GetSetting<bool>("require_email");

        public bool RequirePhone1 => GetSetting<bool>("require_phone1");

        public string RegistrationText => GetSetting<string>("registration_text");

        public bool EnablePatronBranchSelectOption => GetSetting<bool>("enable_patron_branch_select_option");

        public bool BlockOutOfStateRegistrations => GetSetting<bool>("block_out_of_state_registrations");

        public string RegistrationChargeText => GetSetting<string>("registration_charge_text");

        public string RegistrationHeader => GetSetting<string>("registration_form_header");

        public string DuplicatePatronMessageHtml => GetSetting<string>("duplicate_patron_message_html");

        public string KidsFormStreetTwoLabel => GetSetting<string>("kids_form_street_two_label");

        public bool KidsFormRequireStreetTwo => GetSetting<bool>("kids_form_require_street_two");

        public int KidsFormRecordSetId => GetSetting<int>("kids_form_record_set_id");

        public bool KidsFormBypassDupeCheck => GetSetting<bool>("kids_form_bypass_dupe_check");

        public string KidsFormRegistrationText => GetSetting<string>("kids_form_registration_text");

        public string KidsFormDuplicateRegistrationText => GetSetting<string>("kids_form_duplicate_registration_text");

        public bool EnableLegalNameCheckbox => GetSetting<bool>("enable_legal_name_checkbox");

        public string LegalNameCheckboxLabel => GetSetting<string>("legal_name_checkbox_label");

        public bool UseLegalNameOnNotices => GetSetting<bool>("use_legal_name_on_notices");

        public string DriversLicenseButtonText => GetSetting<string>("drivers_license_button_text");
        public string DriversLicensePromptText => GetSetting<string>("drivers_license_prompt_text");
        public string AgreementConfirmButtonText => GetSetting<string>("agreement_confirm_button_text");
        public string AgreementCancelButtonText => GetSetting<string>("agreement_cancel_button_text");
        public string KioskRegistrationText => GetSetting<string>("kiosk_registration_text");
        public string KioskRegistrationHeader => GetSetting<string>("kiosk_registration_header");
        public string SchoolInfoFieldLegend => GetSetting<string>("school_info_field_legend");
        public bool DisplayECardCheckbox => GetSetting<bool>("display_ecard_checkbox");
        public string ECardCheckboxLabel => GetSetting<string>("ecard_checkbox_label");
        public string MailingListDescriptionHtml => GetSetting<string>("mailing_list_description_html");
        public bool DisplayMailingListCheckbox => GetSetting<bool>("display_mailing_list_checkbox");
        public string MailingListCheckboxLabel => GetSetting<string>("mailing_list_checkbox_label");
        public int MailingListRecordSetId => GetSetting<int>("mailing_list_record_set_id");
        public int RegistrationLogonUserId => GetSetting<int>("registration_logon_user_id");
        public int EcardPatronCodeId => GetSetting<int>("ecard_patron_code_id");
        public int TeacherPatronCodeId => GetSetting<int>("teacher_patron_code_id");
        public int StudentPatronCodeId => GetSetting<int>("student_patron_code_id");
        public string SchoolInfoFormat => GetSetting<string>("school_info_format");
        public string ResponsiblePersonDisclaimer => GetSetting<string>("responsible_person_disclaimer");
        public string EcardRegistrationText => GetSetting<string>("ecard_registration_text");
        public string EcardEmailTemplate => GetSetting<string>("ecard_email_template");
        public string EcardEmailFromName => GetSetting<string>("ecard_email_from_name");
        public string EcardEmailSubject => GetSetting<string>("ecard_email_subject");
        public string PostmarkApiKey => GetSetting<string>("postmark_api_key");
        public bool DisplayPreferredPickupLocation => GetSetting<bool>("display_preferred_pickup_location");
        public bool RequirePreferredPickupLocation => GetSetting<bool>("require_preferred_pickup_location");
        public bool DisplayResponsiblePersonField => GetSetting<bool>("display_responsible_person_field");
        public bool PerformPapiDupeBypass => GetSetting<bool>("perform_papi_duplicate_bypass");
        public bool UseFirstNameForDuplicateWorkaround => GetSetting<bool>("use_first_name_for_duplicate_workaround");
    }
}
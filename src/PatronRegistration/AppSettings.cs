﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PatronRegistration
{
    public static class AppSettings
    {
        public static List<int> ShowDlButton
        {
            get
            {
                var raw = ConfigurationManager.AppSettings["show_dl_button"] + ",";
                return raw.Split(',').Where(s => s.Length > 0).Select(s => int.Parse(s)).ToList();
            }
        }

        public static bool AlwaysDupe
        {
            get { var val = false; bool.TryParse(ConfigurationManager.AppSettings["always_dupe"], out val); return val; }
        }

        public static bool Dev => GetValue("dev", false);

        public static string PostmarkApiKey
        {
            get { return ConfigurationManager.AppSettings["postmark_api_key"]; }
        }

        public static string EmailFromAddress
        {
            get { return ConfigurationManager.AppSettings["email_from_address"]; }
        }

        static T GetValue<T>(string key, T defaultValue = default(T))
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(key)) return defaultValue;
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
        }
    }
}
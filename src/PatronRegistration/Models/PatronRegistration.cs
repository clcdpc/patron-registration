﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Clc.Polaris.Api;
using PatronRegistration.Data;
using Clc.Polaris.Api.Models;
using System.Web.Mvc;
using PatronRegistration.Helpers;
using PatronRegistration.Configuration;

namespace PatronRegistration.Models
{
	[MetadataType(typeof(RegistrationMetadata))]
	public class Registration : PatronRegistrationParams
	{
        public int? Day { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }

        public bool DisplayWarning { get; set; }
        public string WarningText { get; set; }
        public bool ZipMismatchRetry { get; set; }

        public bool? ReceiveEreceipts { get; set; }

        public SelectList Branches { get; set; }
        public SelectList PickupBranches { get; set; }




		public bool ShowDlButton { get; set; }
		public string Address2LabelOverride { get; set; }
		public int LibraryId { get; set; }

        public IEnumerable<SelectListItem> Genders { get; set; }

        public bool Retry { get; set; } = false;

		public bool? uateacher { get; set; }
		public bool? uastudent { get; set; }
		public bool IsECard { get; set; }
		public bool AddToMailingList { get; set; }
		public ISettingProvider Settings { get; set; }
	}

	internal sealed class RegistrationMetadata
	{
		public int LogonBranchID { get; set; }
		public int LogonUserID { get; set; }
		public int LogonWorkstationID { get; set; }

		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public int? Day { get; set; }

		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public int? Year { get; set; }

		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public int? Month { get; set; }
		
		[DbConfiguredDisplayName(typeof(Registration))]
		public int PatronBranchID { get; set; }

		//[Display(Name = "Receive receipts for my library transactions electronically?")]
		[Required]
		[DbConfiguredDisplayName(typeof(Registration))]
		public bool? ReceiveEreceipts { get; set; }

		//[Display(Name = "ZIP")]
		[Required]
		[DbConfiguredDisplayName(typeof(Registration))]
		public int? PostalCode { get; set; }
		public int? ZipPlusFour { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public string City { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public string State { get; set; }
		public string County { get; set; }
		public int? CountryID { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		//[Display(Name = "Address line 1")]
		[Required]
		public string StreetOne { get; set; }

		//[Display(Name = "Address line 2")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[DbConfiguredKidsFormRequired]
		public string StreetTwo { get; set; }

		//[Display(Name="First name")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public string NameFirst { get; set; }

		//[Display(Name="Legal First name")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string LegalNameFirst { get; set; }

		//[Display(Name = "Last name")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
		public string NameLast { get; set; }

		//[Display(Name = "Legal Last name")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string LegalNameLast { get; set; }

		//[Display(Name = "MI")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string NameMiddle { get; set; }
		//[Display(Name = "MI")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string LegalNameMiddle { get; set; }
		public string User1 { get; set; }
		public string User2 { get; set; }
		public string User3 { get; set; }
		public string User4 { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		public string User5 { get; set; }
		public string Gender { get; set; }
		public DateTime? Birthdate { get; set; }

		//[Display(Name="Phone 1")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[DbConfiguredRequired("require_phone1")]
		public string PhoneVoice1 { get; set; }

		//[Display(Name = "Phone 2")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string PhoneVoice2 { get; set; }
        
        //[Display(Name = "Email Address")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[DbConfiguredRequired("require_email")]
		public string EmailAddress { get; set; }
		public int? LanguageID { get; set; }

		//[Display(Name="Notification Preference")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[Required]
        public int? DeliveryOptionID { get; set; }
		public string Username { get; set; }

		//[Display(Name="PIN")]
		[DbConfiguredDisplayName(typeof(Registration))]
		[DataType(DataType.Password)]
		[MaxLength(4)]
		[RegularExpression("[0-9][0-9][0-9][0-9]", ErrorMessage="PIN must be numeric")]
		[Required]
		public string Password { get; set; }

		[DbConfiguredDisplayName(typeof(Registration))]
		[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage="Confirmation must match PIN")]
		[DataType(DataType.Password)]
		[Required]
		public string Password2 { get; set; }
		public string AltEmailAddress { get; set; }

		[DbConfiguredDisplayName(typeof(Registration))]
		public int? GenderID { get; set; }

		//[Display(Name = "Phone 3")]
		[DbConfiguredDisplayName(typeof(Registration))]
		public string PhoneVoice3 { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		[DbConfiguredRequired("require_preferred_pickup_location")]
		public int? RequestPickupBranchID { get; set; }
		public int? Phone1CarrierID { get; set; }
		public int? Phone2CarrierID { get; set; }
		public int? Phone3CarrierID { get; set; }
		public bool? EnableSMS { get; set; }
		[DbConfiguredDisplayName(typeof(Registration))]
		public int TxtPhoneNumber { get; set; }
		public string Barcode { get; set; }
		public int? EReceiptOptionID { get; set; }
	}
}
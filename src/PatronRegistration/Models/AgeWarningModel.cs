﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatronRegistration
{
    public class RegistrationAgreementModel
    {
        public int OrganizationID { get; set; }
        public string WarningText { get; set; }
        public string ConfirmButtonText { get; set; }
        public string CancelButtonText { get; set; }
    }
}
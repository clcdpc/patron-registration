﻿namespace PatronRegistration.Models
{
    public class RegistrationSuccessModel
    {
        public string DisplayText { get; set; }
        public int Branch { get; set; }
        public bool ResetForm { get; set; }

    }
}
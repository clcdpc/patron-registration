﻿$(document).ready(function () {
    $('.date').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-110:+10"
    })
});
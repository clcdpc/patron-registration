# README #

## What is this repository for?

* Allows patrons register for a library card using a single page form. Optionally allows filling in information from a driver's license swipe.

## How do I get set up?

#### Pre-Requisites
* AD user account with strong, non-expiring password
* Mandrill API key

#### Installation
* Grant read permissions to the following tables in the Polaris database to the AD user
    * Polaris.AdminAttribute
    * Polaris.Organization
    * Polaris.OrganizationsPPPP
    * Polaris.AdminLanguageString
    * Polaris.SA_CustomMultiLingualStrings
    * Polaris.PatronRegistration
    * Polaris.Patron
    * Polaris.PolarisUserList
    * Polaris.UsersPPPP
* Create HTTPS IIS Site
* Set the application pool Identity to the AD account credentials
* Set 'Start Mode' on the application pool to 'AlwaysRunning'
* Set 'Preload enabled' on the site to 'True'
* Modify the appSettings tag ````<appSettings  file="config\dev.config">```` to load the proper config file, either **dev.config** or **prod.config**
* Make any needed changes to config file
* Publish the project to the IIS site

#### Configuration
* Showing driver's license swipe button
    * Add the ID of the organization(s) that you'd like to display the button to the comma separated list in the 'show_dl_button' setting
* Adding custom header/loading custom css
    * Add entry to the 'org_settings' setting in the **orgSettings.config** file in the following format
        * ````<add orgid="orgid" header_image_url="https://site.com/header.png" css_file="file.css" />````
        * Header image and css file can both be added independently of each other
        * Library IDs can be used to apply settings to all branches